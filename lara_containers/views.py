"""_____________________________________________________________________

:PROJECT: LARA

*lara_containers views *

:details: lara_containers views module.
         - 

:file:    views.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20180627

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

import sys

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse

#~ from django.core.urlresolvers import reverse  # for django 1.10 use : 
from django.urls import reverse

from .models import Container

def index(request):
    container_list = Container.objects.all()
    context = {'container_list': container_list}
    return render(request, 'lara_containers/index.html', context)

def containerList(request):
    container_list = Container.objects.all()
    table_caption = "List of all LARA containers"
    context = {'table_caption': table_caption, 'container_list': container_list}
    return render(request, 'lara_containers/ContainerList.html', context)

def searchForm(request):
    return render(request, 'lara_containers/search.html', context={} )

def search(request):
    search_string = request.POST['search']
    
    try:
        container_list = []
        container = Container.objects.get(name = search_string)
        container_list.append( container  )
        
    except ValueError as err:
        sys.stderr.write("{}".format(err) )
        #~ return render(request, 'polls/detail.html', {
            #~ 'question': question,
            #~ 'error_message': "You didn't select a choice.",
        #~ })
    else:
        context = {'container_list': container_list}
        return render(request, 'lara_containers/results.html', context )
        #return HttpResponseRedirect(reverse('lara_containers:results', args=(container_list,)))
        
def results(request, container_list):
    #question = get_object_or_404(Question, pk=question_id)
    context = {'container_list': container_list}
    return render(request, 'lara_containers/results.html', context )

def containerDetails(request, container_id):
    container = get_object_or_404(Container, pk=container_id)
    context = {'container': container}
    return render(request, 'lara_containers/container.html', context)
