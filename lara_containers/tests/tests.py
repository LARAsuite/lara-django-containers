"""_____________________________________________________________________

:PROJECT: LARA

*lara_containers tests *

:details: lara_containers application tests.
         - 

:file:    tests.py
:authors: mark doerr

:date: (creation)          20180625
:date: (last modification) 20180625

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.test import TestCase
import logging

logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
#~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

# Create your tests here. s. https://docs.djangoproject.com/en/2.0/intro/tutorial05/

import datetime

from django.utils import timezone
from django.test import TestCase

#~ from django.core.urlresolvers import reverse # needs to be changed in dango 1.10 to:
from django.urls import reverse

from django.contrib.auth.models import User

from ..models import Container

def createContainer(num=1):
    """ Creates a Container entry
    """    
    #~ end_of_warrenty_date = datetime.date.today() +  datetime.timedelta(weeks=delta_weeks)
    
    container_list = [ Container.objects.create(description="container description %s" %i  ) for i in range(num) ]
    
    return container_list


class ContainerMethodsTests(TestCase):
    def testContainerCreation(self):
        """ Testing  
        """
        curr_dev = createContainer()
        
        #~ self.assertEqual(curr_dev.name, "test_dev" )
        
class ContainerViewsTests(TestCase):
    def testContainerView(self):
        """ Testing device view
        """
        curr_cont = createContainer()
        
        response = self.client.get(reverse('lara_containers:index'))
        
        #~ self.assertContains(response, "test_dev")
        
    def testSearchFormView(self):
        """ Testing search  view
        """
        curr_dev = createContainer()
        
        response = self.client.get(reverse('lara_containers:searchForm'))
        
        self.assertContains(response, "Search")
