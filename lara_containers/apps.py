"""_____________________________________________________________________

:PROJECT: LARA

*lara_containers apps *

:details: lara_containers configuration. This provides a genaric 
         django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/2.0/ref/applications/
         - 

:file:    apps.py
:authors: mark doerr

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.apps import AppConfig

class LaraContainersConfig(AppConfig):
    name = 'lara_containers'
    lara_app_icon = 'containers_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.

