"""_____________________________________________________________________

:PROJECT: LARA

*lara_containers models *

:details: lara_containers database models. 
         -

:file:    models.py
:authors: mark doerr

:date: (creation)          20180624
:date: (last modification) 20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

import logging
from datetime import date, datetime, timedelta

from django.utils import timezone

from django.db import models

from lara_metainfo.models import MetaInfo
from lara_people.models import Entity, Address

class ExtraDataType(models.Model):
    """EntityExtendedDataType: 
       e.g. email_home, email_lab, telephone_number_home1, company_x_customer_number, 
    """
    def __init__ (self):
        """ Class initialiser """
        extra_type_id =  models.AutoField(primary_key=True)
        description = models.TextField(blank=True, null=True)
        
class ExtraData(models.Model):
    """This class can be used to extend the Entity data, by extra information, 
       e.g. more telephone numbers, customer numbers, ... """
    def __init__ (self):
        """ Class initialiser """
        extra_id =  models.AutoField(primary_key=True)
        data_type = models.ForeignKey(ExtraDataType, related_name="%(app_label)s_%(class)s_extra_data_related", 
                            related_query_name="%(app_label)s_%(class)s_extra_data",on_delete=models.CASCADE, blank=True)
        extra_text = models.TextField(blank=True, null=True) # generic text field
        extra_bin = models.BinaryField(blank=True, null=True) # generic binary field
        extra_file = models.FileField(upload_to='lara_containers/', blank=True, null=True) # rel. path/filename 
        
class ContainerClass(models.Model):
    """ container classes, e.g., MTP, tube, ...  """
    class_id = models.AutoField(primary_key=True)
    name = models.TextField(unique=True)
    description = models.TextField(default="no description", null=True)

    def __str__(self):
        return(self.name) or ''
        
    class Meta:
        db_table = "lara_containers_container_class"
        
class ContainerState(models.Model):
    """ container states, e.g. init, waiting, in-use, ready, processed,  """
    state_id = models.AutoField(primary_key=True)
    state = models.TextField(unique=True)
    description = models.TextField(default="no description", null=True)

    def __str__(self):
        return(self.state) or ''
        
    class Meta:
        db_table = "lara_containers_container_state"

class ContainerLayout(models.Model):
    """ container layout information, e.g. description what individual wells in a microtiterplate contain """
    rows = models.IntegerField(blank=True, null=True)
    cols = models.IntegerField(blank=True, null=True)
    layout = models.BinaryField(blank=True, null=True)
    layout_JSON = models.TextField(blank=True, null=True)
    layout_file = models.FileField(upload_to='lara_containers/layouts', blank=True, null=True)
    vol_unit = models.TextField(default="uL", null=True)
    conc_unit = models.TextField(default="uM", null=True)
    description = models.TextField(default="no description", null=True)

    def __str__(self):
        return(self.description) or ''
        
    class Meta:
        db_table = "lara_containers_container_layout"

class Container(models.Model):
    """ generic container class"""
    container_id = models.AutoField(primary_key=True)
    name = models.TextField(unique=True, null=True)
    barcode = models.TextField(unique=True)
    container_class = models.ForeignKey(ContainerClass, on_delete=models.CASCADE, blank=True, null=True)
    first_used = models.DateTimeField(auto_now_add=True, null=True)
    layout = models.ForeignKey(ContainerLayout,on_delete=models.CASCADE, blank=True, null=True) # default 0
    fabricator = models.ForeignKey('lara_people.Entity', on_delete=models.CASCADE, blank=True, null=True)
    fabrication_date = models.DateField(default="0001-01-01", null=True)
    serial_no = models.TextField(blank=True, null=True)
    purchase_date = models.DateField(default="0001-01-01", null=True)
    state = models.ForeignKey(ContainerState, on_delete=models.CASCADE, blank=True, null=True) # online, waiting, busy, shutting down, off; default: init
    image = models.ImageField(upload_to='lara_containers/images/', blank=True, null=True)
    description = models.TextField(default="no description", null=True)
    metainfo = models.ManyToManyField('lara_metainfo.MetaInfo', related_name='container_metainfo', blank=True) # e.g. tech. specifications, container image
    extra_data = models.ManyToManyField(ExtraData, related_name='%(app_label)s_%(class)s_extradata_related', 
                related_query_name="%(app_label)s_%(class)s_extradata", blank=True) #
    # ',UNIQUE(barcode) ON CONFLICT IGNORE )'),
    
    def __str__(self):
        return(self.name) or ''
 
class ContainerGroup(models.Model):
    """ Container group for combining/grouping containers"""
    container_group_id =  models.AutoField(primary_key=True)
    name = models.TextField(unique=True) # group name
    members = models.ManyToManyField(Container, related_name='container_group', blank=True)
    description = models.TextField(default="no description", null=True)
    
    def __str__(self):
        return(self.name) or ''

    class Meta:
        db_table = "lara_containers_container_group"

