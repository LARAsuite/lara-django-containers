"""_____________________________________________________________________

:PROJECT: LARA

*lara_containers urls *

:details: lara_containers urls module.
         - 

:file:    urls.py
:authors: 

:date: (creation)          
:date: (last modification) 20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.shortcuts import render

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from . import views

app_name = 'lara_containers' # !! this sets the apps namespace to be used in the template

# the 'name' attribute is used in templates to address the url independant of the view
urlpatterns = [
    path('', views.index, name='index'),   # the 'name' value as called by the {% url %} template tag
    #~ path('containerlist$', views.containerList, name='container'),
    #~ path('^(?P<container_id>[0-9]+)/$', views.containerDetails, name='container_details'),
    path('search/', views.searchForm, name='searchForm'),
    path('search-results/', views.search, name='search'),
    path('results/', views.results, name='results'),
    #path('(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    #path('(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    #path('(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

